import React, { Component } from 'react';
import {StyleSheet,TouchableOpacity,Text,View,Image,Dimensions,Alert,} from 'react-native';

import { CommonActions } from '@react-navigation/native';
import { Fumi } from 'react-native-textinput-effects';
import FontAwesomeIcon from 'react-native-vector-icons/Ionicons';
import logo from '../src/img/logo1.png';
import LinearGradient from 'react-native-linear-gradient';

const { width: WIDTH} = Dimensions.get('window')
const { height:HEIGHT} = Dimensions.get('window')


export default class App extends Component {
  constructor(){
    super()
    this.state = {
      press:false,
      showPass:true,
      password: '',
      username: '',
    }
  }

  showPass = () => {
    if (this.state.press == false) {
      this.setState({ showPass:false, press:true })
    } else{
      this.setState({ showPass: true, press: false })
    }
  }
  userChange = user => {
    this.setState({username: user});
  }

  passChange = pass => {
    this.setState({password: pass});
  }

  showAlert = () => {
    Alert.alert(
      'Upps!',
      'Las credenciales son incorrectas',
      [
        {text: 'Ok',}
      ],
      {cancelable: false},
    );
  }

  render() {
    return (
      <LinearGradient colors={['#FFA000', '#FFCA28', '#FFA000']}>
      <View style={styles.container}>
        

        <View style={{alignContent:'center',justifyContent:'center', paddingLeft:'10%',paddingRight:'10%'}}>
          <Image source={logo} style={{height:'50%',width:'100%',paddingLeft:'10%',paddingRight:'10%'}} /> 
        </View>
        <View style={styles.inputContainer}>
          <Fumi
            label={'Usuario'}
            iconClass={FontAwesomeIcon}
            iconName={'ios-person'}
            iconColor={'#000000'}
            iconSize={20}
            iconWidth={40}
            onChangeText={user => this.userChange(user)}
            inputPadding={16}
            style={{borderRadius:5,color:'#000000'}}
          />
        </View>

        <View style={styles.inputContainer}>
          
          <Fumi
            label={'Contraseña'}
            iconClass={FontAwesomeIcon}
            iconName={'ios-lock'}
            iconColor={'#000000'}
            iconSize={20}
            iconWidth={40}
            onChangeText={pass => this.passChange(pass)}
            secureTextEntry={this.state.showPass}
            inputPadding={16}
            style={{borderRadius:5, color:'#000000'}}
          />

          <TouchableOpacity style={styles.btnEye} onPress={this.showPass.bind(this)}>
            <Text style={{color:'#000000'}}>{this.state.press == false ? 'MOSTRAR' : 'OCULTAR'}</Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.btnLogin}>
          <Text style={styles.text} 
            onPress={
              this.state.password == '123' && this.state.username == 'Neo' ? 
              () => this.props.navigation.dispatch(
                CommonActions.reset({
                  index: 1,
                  routes: [
                    { name: 'Home' },
                  ],
                })
              ) : 
              this.showAlert}
            >Iniciar Sesión</Text>
        </TouchableOpacity>
      </View>
      </LinearGradient>
    );
  }
} 

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container:{
    height:HEIGHT,
    justifyContent: 'center',
  },
  logoContainer: {
    alignItems: 'center',
  },
  logo:{
    width: '100%',
    height: '41%',
  },

  input:{
    width:WIDTH-55,
    height:45,
    borderRadius:25,
    fontSize:16,
    paddingLeft:45,
    color: '#000000',
    marginHorizontal:25,
  },
  inputIcon:{
    position: 'absolute',
    top: 8,
    left: 37
  },
  inputContainer:{
    marginTop:5,
    marginLeft: '6%',
    marginRight: '6%',
    color:'#000000',
  },
  btnEye:{
    position: 'absolute',
    top: '32%',
    right: 25,
  },
  btnLogin:{
    width: WIDTH - 55,
    height: 45,
    borderRadius: 5,
    backgroundColor: '#222222',
    justifyContent: 'center',
    marginTop: 20,
    marginLeft: '6%',
    marginRight: '6%',
    borderColor: 'white',
    borderWidth: 1,
  },
  text:{
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 16,
    textAlign: 'center',
  },
});