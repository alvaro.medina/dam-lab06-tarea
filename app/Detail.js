import React, { Component } from 'react';
import {View,StyleSheet,Text,Image,TouchableOpacity,ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import I from "react-native-vector-icons/Entypo";
import LinearGradient from 'react-native-linear-gradient';

export default class ConexionFetch extends Component{
    constructor(props){
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
            id: this.props.route.params.itemId,
        };
    }
    async componentDidMount() {
        await fetch(`https://yts.mx/api/v2/movie_details.json?movie_id=${this.state.id}`)
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        items: result.data.movie,
                    });
                },
                error => {
                    this.setState({
                        error: error,
                    });
                },
            );
    }

    render(){
        return(
            
            <LinearGradient style={styles.container} colors={['#FFA000', '#FFCA28', '#FFA000']}>
               
                <ScrollView>
                <View>
                    <Image source={{uri: this.state.items.background_image_original}} style={styles.itemBackground}
                    resizeMode="cover"/>
                    <Image source={{uri: this.state.items.medium_cover_image}} style={styles.itemImage} resizeMode="cover"/>
                </View>
                <Text></Text>
                <Text></Text>
                <Text></Text>
                <View style={styles.top}>
                <View style={styles.staranting}>

                <Text style={styles.title2} >Rating: </Text>
                                <I
                                    name="star"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}
                                />
                                <I
                                    name="star"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}

                                />
                                <I
                                    name="star"
                                    color="#fff"
                                    size={25}
                                    style={styles.star}

                                />
                                <Text style={styles.rating}>{this.state.items.rating}</Text>
                            </View>
                </View>
                <View style={styles.text}>
                    <TouchableOpacity style={styles.button}>
                        <Icon
                            name="play"
                            color="#fff"
                            size={22}
                            style={styles.icon}/>
                        <Text style={styles.play}>Ver Trailer</Text>
                    </TouchableOpacity>
                    <View style={styles.viewTitle}>
                        <View style={styles.tit}>
                            <View>
                                <Text style={styles.titleTop}>{this.state.items.title}
                                    <Text style={styles.year}> ({this.state.items.year})</Text>
                                </Text>
                            </View>
                        </View>
                        <Text style={styles.title3}>Sinopsis:</Text>
                        <Text style={styles.title}>{this.state.items.description_intro}</Text>
                    </View>
                </View>
                </ScrollView>
               
            </LinearGradient>
        
        );
    }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F57C00',
        flex: 1,
    },
    itemContainer: {
        flex: 1,
    },
    button:{
        flexDirection: 'row',
        backgroundColor: '#43A047',
        marginLeft: '5%',
        marginRight: '5%',
        padding: '3%',
        borderRadius: 50,
        justifyContent: 'center',    
    },
    itemImage: {
        top: '25%',
        left: '35%',
        width: '30%',
        height: '115%',
        position: 'absolute',
        borderRadius: 5,
    },
    itemBackground: {
        width: '100%',
        height: 180,
    },
    title:{
        color: 'white',
        textAlign: 'justify',
        marginTop: '2%',
        fontSize:16,
    },
    title2:{
        color: 'white',
        textAlign: 'justify',
        marginTop: '2%',
    },
    title3:{
        color: 'white',
        textAlign: 'justify',
        marginTop: '2%',
        fontSize:22,
    },
    text:{
        paddingTop: '7%',
    },
    icon:{
        marginTop: 2,
        marginRight: 10,
    },
    play:{
        color: 'white',
        fontSize: 18,
    },
    top:{
        marginLeft: '33%',
        marginTop: '5%',
        flexDirection: 'row',
    },
    viewTitle:{
        marginLeft: '5%',
        marginRight: '5%',
    },
    titleTop:{
        color: 'white',
        marginTop: '2%',
        marginRight: '12%',
        fontSize: 40,
        fontFamily: 'cursive',
    },
    tit:{
    },
    star:{
    },  
    rating:{
        marginRight: '1%',
        fontSize: 18,
        color: 'white',
        fontFamily: 'arial',
        marginLeft: '3%',
    },
    staranting:{
        flexDirection: 'row',
    },
    year:{
        color: 'white',
        marginBottom: '2%',
        marginRight: '12%',
        fontSize: 20,
        fontFamily: 'cursive',
    },
});
